import React from 'react'
import Swal from 'sweetalert2'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';

const AdminLeagues = () => {
  const [apuesta, setApuesta] = React.useState([]);

  const handleChange = (event, key) => {
    let auxApuesta = apuesta
    auxApuesta[key] = event.target.value
    setApuesta(auxApuesta);
  };

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  function createData(name, calories) {
    return { name, calories };
  }

  const rows = [
    createData('Frozen yoghurt', 'Ice cream sandwich'),
    createData('Ice cream sandwich', 'Eclair'),
    createData('Eclair', 'Gingerbread'),
    createData('Cupcake', 'Ice cream sandwich'),
    createData('Gingerbread', 'Cupcake'),
    createData('Frozen yoghurt', 'Ice cream sandwich'),
    createData('Ice cream sandwich', 'Eclair'),
    createData('Eclair', 'Gingerbread'),
    createData('Cupcake', 'Ice cream sandwich'),
    createData('Gingerbread', 'Cupcake'),
    createData('Frozen yoghurt', 'Ice cream sandwich'),
    createData('Ice cream sandwich', 'Eclair'),
    createData('Eclair', 'Gingerbread'),
    createData('Cupcake', 'Ice cream sandwich'),
    createData('Gingerbread', 'Cupcake'),
    createData('Frozen yoghurt', 'Ice cream sandwich'),
  ];

  return (
    <div style={{ marginTop: '50px' }}>
      <div>
        <h3 style={{ textAlign: 'center', marginBottom: '20px' }}>Elección de equipos</h3>
      </div>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <div style={{ position: 'relative', padding: '30px', width: '50%' }}>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Equipo Local</StyledTableCell>
                  <StyledTableCell align="center">Apuesta</StyledTableCell>
                  <StyledTableCell align="right">Equipo Visitante</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row, key) => (
                  <StyledTableRow key={row.name}>
                    <StyledTableCell component="th" scope="row">{row.name}</StyledTableCell>
                    <StyledTableCell align="center">
                      <Box sx={{ minWidth: 120 }}>
                        <FormControl fullWidth>
                          <InputLabel id="demo-simple-select-label">Elección</InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={apuesta[key]}
                            label="Apuesta"
                            onChange={handleChange}
                          >
                            <MenuItem value={10}>GANA LOCAL</MenuItem>
                            <MenuItem value={20}>EMPATE</MenuItem>
                            <MenuItem value={30}>GANA VISITA</MenuItem>
                          </Select>
                        </FormControl>
                      </Box>
                    </StyledTableCell>
                    <StyledTableCell align="right">{row.calories}</StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <div style={{ display: 'flex', justifyContent: 'center', marginTop: '15px' }}>
            <Button style={{ background: '#ffdc00', color: 'black' }} variant="contained">ENVIAR APUESTA</Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AdminLeagues