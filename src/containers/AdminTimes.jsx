import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { listSeasons, listLeagues, listTimes, registerTime, updateTime, deleteTime } from '../utils/actions'

const AdminTimes = () => {
  const [leagues, setLeagues] = useState([])
  const [seasons, setSeasons] = useState([])
  const [times, setTimes] = useState([])

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const agregarJornada = () => {
    Swal.fire({
      title: "Partidos por jornada",
      html: `
            <h5>Liga</h5>
            <select style="width: 263px; margin: 1px;" name="liga_id" id="liga_id" class="swal2-input">
            ${leagues?.map((league) => {
        return (
          `<option value="${league.liga_id}"}>${league.nombre}</option>`
        )
      })}
            </select>
            <h5>Temporada</h5>
            <select style="width: 263px; margin: 1px;" name="temporada_id" id="temporada_id" class="swal2-input">
            ${seasons?.map((season) => {
        return (
          `<option value="${season.temporada_id}"}>${season.numero}</option>`
        )
      })}
            </select>
            <h5>Numero de la Jornada</h5>
            <input type="text" id="numero"  style="width:58%; margin: 1px;"      class="swal2-input" placeholder="">
            <h5>Nombre de la Jornada</h5>
            <input type="text" id="nombre"  style="width:58%; margin: 1px;"      class="swal2-input" placeholder="">
            <h5>Fecha inicio</h5>
            <input type="date" id="fecha_inicio" style="width: 58%; margin: 1px;"    class="swal2-input" placeholder="Fecha inicio">
            <h5>Fecha fin</h5>
            <input type="date" id="fecha_fin" style="width: 58%; margin: 1px;"    class="swal2-input" placeholder="Fecha Terminacion">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const liga = Swal.getPopup().querySelector("#liga_id").value;
        const temporada = Swal.getPopup().querySelector("#temporada_id").value;
        const numero = Swal.getPopup().querySelector("#numero").value;
        const nombre = Swal.getPopup().querySelector("#nombre").value;
        const fechaInicio = Swal.getPopup().querySelector("#fecha_inicio").value;
        const fechaFin = Swal.getPopup().querySelector("#fecha_fin").value;

        if (!liga || !temporada || !numero || !nombre || !fechaInicio || !fechaFin) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }

        return { numero, nombre, liga_id: liga, f_inicio: fechaInicio, f_fin: fechaFin, temporada_id: temporada };
      },
    }).then(async (result) => {
      if (result?.value) {
        await registerTime(result.value)
        await obtenerJornadas()
      }
    });
  }

  const editarJornada = (row) => {
    Swal.fire({
      title: `Editar partido por jornada`,
      html: `
      <h5>Liga</h5>
      <select style="width: 263px; margin: 1px;" name="liga_id" id="liga_id" class="swal2-input">
      ${leagues?.map((league) => {
        return (
          `<option value="${league.liga_id}" ${row?.liga_id === league?.liga_id && 'selected'}>${league.nombre}</option>`
        )
      })}
      </select>
      <h5>Temporada</h5>
      <select style="width: 263px; margin: 1px;" name="temporada_id" id="temporada_id" class="swal2-input">
      ${seasons?.map((season) => {
        return (
          `<option value="${season.temporada_id}" ${row?.temporada_id === season?.temporada_id && 'selected'}>${season.numero}</option>`
        )
      })}
      </select>
      <h5>Numero de la Jornada</h5>
      <input type="text" id="numero"  style="width:58%; margin: 1px;"      class="swal2-input" placeholder="" value="${row?.numero}">
      <h5>Nombre de la Jornada</h5>
      <input type="text" id="nombre"  style="width:58%; margin: 1px;"      class="swal2-input" placeholder="" value="${row?.nombre}">
      <h5>Fecha inicio</h5>
      <input type="date" id="fecha_inicio" style="width: 58%; margin: 1px;" id="password"    class="swal2-input" placeholder="Fecha inicio">
      <h5>Fecha fin</h5>
      <input type="date" id="fecha_fin" style="width: 58%; margin: 1px;" id="password"    class="swal2-input" placeholder="Fecha Terminacion">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const liga = Swal.getPopup().querySelector("#liga_id").value;
        const temporada = Swal.getPopup().querySelector("#temporada_id").value;
        const numero = Swal.getPopup().querySelector("#numero").value;
        const nombre = Swal.getPopup().querySelector("#nombre").value;
        const fechaInicio = Swal.getPopup().querySelector("#fecha_inicio").value;
        const fechaFin = Swal.getPopup().querySelector("#fecha_fin").value;

        if (!liga || !temporada || !numero || !nombre || !fechaInicio || !fechaFin) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }

        return { numero, nombre, liga_id: liga, f_inicio: fechaInicio, f_fin: fechaFin, temporada_id: temporada, jornada_id: row.jornada_id };
      },
    }).then(async (result) => {
      if (result?.value) {
        await updateTime(result.value)
        await obtenerJornadas()
      }
    });
  }

  const eliminarJornada = (row) => {
    Swal.fire({
      title: `Quieres eliminar la jornada (${row.nombre})?`,
      showCancelButton: true,
      showDenyButton: false,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then(async (result) => {
      await deleteTime(row.jornada_id)
      await obtenerJornadas()
    })
  }

  const obtenerLigas = async () => {
    let response = await listLeagues()

    response && setLeagues(response)
  }

  const obtenerTemporadas = async () => {
    let response = await listSeasons()

    response && setSeasons(response)
  }

  const obtenerJornadas = async () => {
    let response = await listTimes()

    response && setTimes(response)
  }

  useEffect(() => {
    obtenerJornadas()
    obtenerTemporadas()
    obtenerLigas()
  }, [])

  return (
    <div style={{ marginTop: '50px' }}>
      <div>
        <h3 style={{ textAlign: 'center', marginBottom: '20px' }}>Jornadas</h3>
      </div>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <div style={{ position: 'relative', padding: '30px', width: '80%' }}>
          <span onClick={agregarJornada} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
            <i class="fa-solid fa-plus"></i> Agregar
          </span>
          {
            times?.length === 0 ? <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <p>Actualmente no tienes jornadas registradas, ingresa una.</p>
              <span onClick={agregarJornada} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
                <i class="fa-solid fa-plus"></i> Agregar
              </span>
            </div> :
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell>ID liga</StyledTableCell>
                      <StyledTableCell align="right"># de la temporada</StyledTableCell>
                      <StyledTableCell align="right">ID jornada o fecha</StyledTableCell>
                      <StyledTableCell align="right">Nombre de la fecha o jornada</StyledTableCell>
                      <StyledTableCell align="right">Fecha de inicio</StyledTableCell>
                      <StyledTableCell align="right">Fecha de terminacion</StyledTableCell>
                      <StyledTableCell align="right">Acciones</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {times?.length !== 0 && times?.map((row, key) => (
                      <StyledTableRow key={key}>
                        <StyledTableCell component="th" scope="row">{row.liga_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.temporada_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.jornada_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.nombre}</StyledTableCell>
                        <StyledTableCell align="right">{row.fecha_inicio}</StyledTableCell>
                        <StyledTableCell align="right">{row.fecha_fin}</StyledTableCell>
                        <StyledTableCell align="right">
                          <div>
                            <i style={{ marginRight: '15px' }} onClick={() => editarJornada(row)} class="fa-solid fa-pencil"></i>
                            <i class="fa-solid fa-trash-can" onClick={() => eliminarJornada(row)}></i>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
          }
        </div>
      </div>
    </div>
  )
}

export default AdminTimes