import React from 'react';
import { withRouter } from 'react-router-dom';
import '../styles/components/Dashboard.css'
import Liga from '../assets/img/liga.jpg'
import Temporada from '../assets/img/temp.jpg'
import Equipo from '../assets/img/equipos.jpg'
import Jornada from '../assets/img/jornada.jpg'
import PartidosPorJornada from '../assets/img/partidosporjornada.jpg'

const Dashboard = (props) => {

  return (
    <>
      <div style={{ marginTop: '70px' }}>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          <div className="card-flex">
            <div className="card" onClick={() => { props.history.push('/admin/dashboard/ligas') }}>
              <img style={{ width: '350px', height: '190px', borderRadius: '40px 40px 0px 0px' }} src={Liga} alt="Card image cap" />
              <div style={{ height: '35px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h5 className="card-title">Ligas</h5>
              </div>
            </div>
          </div>

          <div className="card-flex">
            <div className="card" onClick={() => { props.history.push('/admin/dashboard/temporadas') }}>
              <img style={{ width: '350px', height: '190px', borderRadius: '40px 40px 0px 0px' }} src={Temporada} alt="Card image cap 2" />
              <div style={{ height: '35px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h5 className="card-title">Temporadas</h5>
              </div>
            </div>
          </div>

          <div className="card-flex">
            <div className="card" onClick={() => { props.history.push('/admin/dashboard/equipos') }}>
              <img style={{ width: '350px', height: '190px', borderRadius: '40px 40px 0px 0px' }} src={Equipo} alt="Card image cap 3" />
              <div style={{ height: '35px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h5 className="card-title">Equipos</h5>
              </div>
            </div>
          </div >
        </div >

        <div style={{ display: 'flex', flexWrap: 'wrap', marginTop: '70px' }}>
          <div className="card-flex">
            <div className="card" onClick={() => { props.history.push('/admin/dashboard/jornadas') }}>
              <img style={{ width: '350px', height: '190px', borderRadius: '40px 40px 0px 0px' }} src={Jornada} alt="Card image cap" />
              <div style={{ height: '35px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h5 className="card-title">Jornadas</h5>
              </div>
            </div>
          </div>
          <div className="card-flex">
            <div className="card" onClick={() => { props.history.push('/admin/dashboard/partidos-por-jornadas') }}>
              <img style={{ width: '350px', height: '190px', borderRadius: '40px 40px 0px 0px' }} src={PartidosPorJornada} alt="Card image cap" />
              <div style={{ height: '35px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h5 className="card-title">Partidos Por Jornada</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default withRouter(Dashboard);