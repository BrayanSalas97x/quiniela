import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { registerLeague, listLeagues, url, updateLeague, deleteLeague } from '../utils/actions'

const AdminLeagues = () => {
  const [leagues, setLeagues] = useState([])

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
      width: '33%'
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const agregarLiga = () => {
    Swal.fire({
      title: "Selecciona el Logo de la liga",
      html: `<input type="text" id="nombre_liga" class="swal2-input" placeholder="Nombre de la Liga">
             <input type="file" id="file" style="width: 58%;font-size: 15px;" class="swal2-input" placeholder="Logo del Equipo">`,
      inputAttributes: {
        accept: "image/*",
        "aria-label": "",
      },
      preConfirm: () => {
        if (!document.getElementById('nombre_liga').value || !document.getElementById('file').files) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }
        return {
          nombre: document.getElementById('nombre_liga').value,
          logo: document.getElementById('file').files[0],
          active: true
        }
      }
    }).then(async (result) => {
      if (result?.value) {
        await registerLeague(result.value)
        await obtenerLigas()
      }
    })
  }

  const editarLiga = (row) => {
    Swal.fire({
      title: `Editar liga ${row.nombre}`,
      html: `<input type="text" id="nombre_liga" class="swal2-input" placeholder="Nombre de la Liga" value="${row.nombre}">
              <input type="file" id="file" style="width: 58%;font-size: 15px;" class="swal2-input" placeholder="Logo del Equipo">`,
      inputAttributes: {
        accept: "image/*",
        "aria-label": "",
      },
      preConfirm: () => {
        if (!document.getElementById('nombre_liga').value || !document.getElementById('file').files) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }
        return {
          nombre: document.getElementById('nombre_liga').value,
          logo: document.getElementById('file').files[0],
          id: row.liga_id,
          active: true
        }
      }
    }).then(async (result) => {
      console.log(result.value, 'los valores...')
      if (result?.value) {
        await updateLeague(result.value)
        await obtenerLigas()
      }
    })
  }

  const eliminarLiga = (row) => {
    Swal.fire({
      title: `Quieres eliminar esta liga (${row.nombre})?`,
      showCancelButton: true,
      showDenyButton: false,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then(async (result) => {
      if (result.isConfirmed) {
        await deleteLeague({ id: row.liga_id })
        await obtenerLigas()
      }
    })
  }

  const obtenerLigas = async () => {
    let response = await listLeagues()

    response && setLeagues(response)
  }

  useEffect(() => {
    obtenerLigas()
  }, [])

  return (
    <div style={{ marginTop: '50px' }}>
      <div>
        <h3 style={{ textAlign: 'center', marginBottom: '20px' }}>Ligas</h3>
      </div>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <div style={{ position: 'relative', padding: '30px', width: '80%' }}>
          <span onClick={agregarLiga} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
            <i class="fa-solid fa-plus"></i> Agregar
          </span>
          {
            leagues?.length === 0 ?
              <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <p>Actualmente no tienes ligas registradas, ingresa una.</p>
                <span onClick={agregarLiga} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
                  <i class="fa-solid fa-plus"></i> Agregar
                </span> 
              </div> :
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell>Imagen de la Liga</StyledTableCell>
                      <StyledTableCell align="center">Nombre de la Liga</StyledTableCell>
                      <StyledTableCell align="right">Acciones</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {leagues?.length !== 0 && leagues?.map((league) => (
                      <StyledTableRow key={league.nombre}>
                        <StyledTableCell component="th" scope="row"><img style={{ width: '60px' }} src={`${url}${league.logo}`}></img>{ }</StyledTableCell>
                        <StyledTableCell align="center">{league.nombre}</StyledTableCell>
                        <StyledTableCell align="right">
                          <div>
                            <i style={{ marginRight: '15px', cursor: 'pointer' }} class="fa-solid fa-pencil" onClick={() => editarLiga(league)}></i>
                            <i style={{ cursor: 'pointer' }} class="fa-solid fa-trash-can" onClick={() => eliminarLiga(league)}></i>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
          }
        </div>
      </div>
    </div>
  )
}

export default AdminLeagues