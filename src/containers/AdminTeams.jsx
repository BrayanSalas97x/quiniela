import React, { useState, useEffect } from 'react'
import Swal from 'sweetalert2'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { listSeasons, listTeams, listLeagues, url, registerTeam, updateTeam, deleteTeam } from '../utils/actions'


const AdminTeams = () => {
  const [leagues, setLeagues] = useState([])
  const [seasons, setSeasons] = useState([])
  const [teams, setTeams] = useState([])

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const agregarEquipo = () => {
    Swal.fire({
      title: "Agregar Equipo",
      html: `
            <h5>ID de Liga</h5>
            <select style="width: 263px; margin: 1px;" name="liga_id" id="liga_id" class="swal2-input">
            ${leagues?.map((league) => {
        return (
          `<option value="${league.liga_id}"}>${league.nombre}</option>`
        )
      })}
            </select>
            <h5># de la Temporada</h5>
            <select style="width: 263px; margin: 1px;" name="temporada_id" id="temporada_id" class="swal2-input">
            ${seasons?.map((season) => {
        return (
          `<option value="${season.temporada_id}"}>${season.numero}</option>`
        )
      })}
            </select>
            <h5 style="margin:6px;">Nombre del Equipo</h5>
            <input type="text"style="width:58%; margin: 1px;" id="nombre"   class="swal2-input" placeholder="">
            <h5>Logo del Equipo</h5>
            <input type="file" id="file" style="width: 58% ;font-size: 15px;" class="swal2-input" placeholder="">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        if (!document.getElementById('liga_id').value || !document.getElementById('file').files || !document.getElementById('temporada_id').value || !document.getElementById('nombre').value) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }
        return {
          liga_id: document.getElementById('liga_id').value,
          logo: document.getElementById('file').files[0],
          temporada_id: document.getElementById('temporada_id').value,
          nombre: document.getElementById('nombre').value,
          activo: true
        }
      },
    }).then(async (result) => {
      if (result?.value) {
        await registerTeam(result.value)
        await obtenerEquipos()
      }
    });
  }

  const editarEquipo = (row) => {
    Swal.fire({
      title: `Editar Equipo ${row.nombre}`,
      html: `
      <h5>ID de Liga</h5>
      <select style="width: 263px; margin: 1px;" name="liga_id" id="liga_id" class="swal2-input">
      ${leagues?.map((league) => {
        return (
          `<option value="${league?.liga_id}" ${row?.liga_id === league?.liga_id && 'selected'}>${league?.nombre}</option>`
        )
      })}
      </select>
      <h5># de la Temporada</h5>
      <select style="width: 263px; margin: 1px;" name="temporada_id" id="temporada_id" class="swal2-input">
      ${seasons?.map((season) => {
        return (
          `<option value="${season?.temporada_id}" ${row?.temporada_id === season?.temporada_id && 'selected'}>${season?.numero}</option>`
        )
      })}
      </select>
      <h5 style="margin:6px;">Nombre del Equipo</h5>
      <input type="text"style="width:58%; margin: 1px;" id="nombre"   class="swal2-input" placeholder="" value="${row.nombre}">
      <h5>Logo del Equipo</h5>
      <input type="file" id="file" style="width: 58% ;font-size: 15px;" class="swal2-input" placeholder="">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        if (!document.getElementById('liga_id').value || !document.getElementById('file').files || !document.getElementById('temporada_id').value || !document.getElementById('nombre').value) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }
        return {
          equipo_id: row.equipo_id,
          liga_id: document.getElementById('liga_id').value,
          logo: document.getElementById('file').files[0],
          temporada_id: document.getElementById('temporada_id').value,
          nombre: document.getElementById('nombre').value,
          activo: true
        }
      },
    }).then(async (result) => {
      if (result?.value) {
        await updateTeam(result.value)
        await obtenerEquipos()
      }
    });
  }

  const eliminarEquipo = (row) => {
    Swal.fire({
      title: `Quieres eliminar el equipo (${row.nombre})?`,
      showCancelButton: true,
      showDenyButton: false,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then(async (result) => {
      await deleteTeam(row.equipo_id)
      await obtenerEquipos()
    })
  }

  const obtenerLigas = async () => {
    let response = await listLeagues()

    response && setLeagues(response)
  }

  const obtenerTemporadas = async () => {
    let response = await listSeasons()

    response && setSeasons(response)
  }

  const obtenerEquipos = async () => {
    let response = await listTeams()

    response && setTeams(response)
  }

  useEffect(() => {
    obtenerEquipos()
    obtenerTemporadas()
    obtenerLigas()
  }, [])

  return (
    <div style={{ marginTop: '50px' }}>
      <div>
        <h3 style={{ textAlign: 'center', marginBottom: '20px' }}>Equipos</h3>
      </div>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <div style={{ position: 'relative', padding: '30px', width: '80%' }}>
          <span onClick={agregarEquipo} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
            <i class="fa-solid fa-plus"></i> Agregar
          </span>
          {
            teams?.length === 0 ? <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <p>Actualmente no tienes equipos registrados, ingresa uno.</p>
              <span onClick={agregarEquipo} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
                <i class="fa-solid fa-plus"></i> Agregar
              </span>
            </div> :
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell align="left">ID liga</StyledTableCell>
                      <StyledTableCell align="right"># de la temporada</StyledTableCell>
                      <StyledTableCell align="right">ID equipo</StyledTableCell>
                      <StyledTableCell align="right">Nombre del equipo</StyledTableCell>
                      <StyledTableCell align="right">Logo del equipo</StyledTableCell>
                      <StyledTableCell align="right">Acciones</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {teams?.length !== 0 && teams?.map((row, key) => (
                      <StyledTableRow key={key}>
                        <StyledTableCell component="th" scope="row">{row.liga_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.temporada_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.equipo_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.nombre}</StyledTableCell>
                        <StyledTableCell align="right"><img style={{ width: '60px' }} src={`${url}${row.logo}`}></img>{ }</StyledTableCell>
                        <StyledTableCell align="right">
                          <div>
                            <i style={{ marginRight: '15px' }} onClick={() => editarEquipo(row)} class="fa-solid fa-pencil"></i>
                            <i class="fa-solid fa-trash-can" onClick={() => eliminarEquipo(row)}></i>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
          }
        </div>
      </div>
    </div>
  )
}

export default AdminTeams