import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { listSeasons, listLeagues, listTimes, listTeams, listMatches, registerMatch, updateMatch, deleteMatch } from '../utils/actions'

const AdminMatches = () => {
  const [leagues, setLeagues] = useState([])
  const [seasons, setSeasons] = useState([])
  const [times, setTimes] = useState([])
  const [teams, setTeams] = useState([])
  const [matches, setMatches] = useState([])

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


  const agregarPartidosPorJornada = () => {
    Swal.fire({
      title: "Partidos por jornada",
      html: `
      <h5>Liga</h5>
      <select style="width: 263px; margin: 1px;" name="liga_id" id="liga_id" class="swal2-input">
      ${leagues?.map((league) => {
        return (
          `<option value="${league.liga_id}"}>${league.nombre}</option>`
        )
      })}
      </select>
      <h5>Temporada</h5>
      <select style="width: 263px; margin: 1px;" name="temporada_id" id="temporada_id" class="swal2-input">
      ${seasons?.map((season) => {
        return (
          `<option value="${season.temporada_id}"}>${season.numero}</option>`
        )
      })}
      </select>
      <h5>Jornada</h5>
      <select style="width: 263px; margin: 1px;" name="jornada_id" id="jornada_id" class="swal2-input">
      ${times?.map((time) => {
        return (
          `<option value="${time.jornada_id}"}>${time.nombre}</option>`
        )
      })}
      </select>
      <h5>Equipo Local</h5>
      <select style="width: 263px; margin: 1px;" name="local_id" id="local_id" class="swal2-input">
      ${teams?.map((team) => {
        return (
          `<option value="${team.equipo_id}"}>${team.nombre}</option>`
        )
      })}
      </select>
      <h5>Equipo Visitante</h5>
      <select style="width: 263px; margin: 1px;" name="visitante_id" id="visitante_id" class="swal2-input">
      ${teams?.map((team) => {
        return (
          `<option value="${team.equipo_id}"}>${team.nombre}</option>`
        )
      })}
      </select>
      <h5>Jornada original</h5>
      <input type="text" id="jornada_original"  style="width:58%; margin: 1px;"      class="swal2-input" placeholder="">
      `,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const liga = Swal.getPopup().querySelector("#liga_id").value;
        const temporada = Swal.getPopup().querySelector("#temporada_id").value;
        const jornada = Swal.getPopup().querySelector("#jornada_id").value;
        const visitanteId = Swal.getPopup().querySelector("#visitante_id").value;
        const localId = Swal.getPopup().querySelector("#local_id").value;
        const jornadaOriginal = Swal.getPopup().querySelector("#jornada_original").value;

        if (!liga || !temporada || !jornada || !visitanteId || !localId || !jornadaOriginal) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }

        return { liga_id: liga, temporada_id: temporada, jornada_id: jornada, vistante_id: visitanteId, local_id: localId, jornada_original: jornadaOriginal };
      },
    }).then(async (result) => {
      if (result?.value) {
        await registerMatch(result.value)
        await obtenerPartidos()
      }
    });
  }

  const editarPartidosPorJornada = (row) => {
    Swal.fire({
      title: "Editar Partidos por jornada",
      html: `
      <h5>Liga</h5>
      <select style="width: 263px; margin: 1px;" name="liga_id" id="liga_id" class="swal2-input">
      ${leagues?.map((league) => {
        return (
          `<option value="${league.liga_id}" ${row?.liga_id === league?.liga_id && 'selected'}>${league.nombre}</option>`
        )
      })}
      </select>
      <h5>Temporada</h5>
      <select style="width: 263px; margin: 1px;" name="temporada_id" id="temporada_id" class="swal2-input">
      ${seasons?.map((season) => {
        return (
          `<option value="${season.temporada_id}" ${row?.temporada_id === season?.temporada_id && 'selected'}>${season.numero}</option>`
        )
      })}
      </select>
      <h5>Jornada</h5>
      <select style="width: 263px; margin: 1px;" name="jornada_id" id="jornada_id" class="swal2-input">
      ${times?.map((time) => {
        return (
          `<option value="${time.jornada_id}" ${row?.jornada_id === time?.jornada_id && 'selected'}>${time.nombre}</option>`
        )
      })}
      </select>
      <h5>Equipo Local</h5>
      <select style="width: 263px; margin: 1px;" name="local_id" id="local_id" class="swal2-input">
      ${teams?.map((team) => {
        return (
          `<option value="${team.equipo_id}" ${row?.local_id === team?.equipo_id && 'selected'}>${team.nombre}</option>`
        )
      })}
      </select>
      <h5>Equipo Visitante</h5>
      <select style="width: 263px; margin: 1px;" name="visitante_id" id="visitante_id" class="swal2-input">
      ${teams?.map((team) => {
        return (
          `<option value="${team.equipo_id}" ${row?.vistante_id === team?.equipo_id && 'selected'}>${team.nombre}</option>`
        )
      })}
      </select>
      <h5>Jornada original</h5>
      <input type="text" id="jornada_original"  style="width:58%; margin: 1px;"      class="swal2-input" placeholder="" value="${row?.jornada_original}">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const liga = Swal.getPopup().querySelector("#liga_id").value;
        const temporada = Swal.getPopup().querySelector("#temporada_id").value;
        const jornada = Swal.getPopup().querySelector("#jornada_id").value;
        const visitanteId = Swal.getPopup().querySelector("#visitante_id").value;
        const localId = Swal.getPopup().querySelector("#local_id").value;
        const jornadaOriginal = Swal.getPopup().querySelector("#jornada_original").value;

        if (!liga || !temporada || !jornada || !visitanteId || !localId || !jornadaOriginal) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }

        return { liga_id: liga, temporada_id: temporada, jornada_id: jornada, vistante_id: visitanteId, local_id: localId, jornada_original: jornadaOriginal, partido_id: row.partido_id };
      },
    }).then(async (result) => {
      if (result?.value) {
        await updateMatch(result.value)
        await obtenerPartidos()
      }
    });
  }

  const eliminarPartidosPorJornada = (row) => {
    Swal.fire({
      title: `Quieres eliminar este partido?`,
      showCancelButton: true,
      showDenyButton: false,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then(async (result) => {
      await deleteMatch(row.partido_id)
      await obtenerPartidos()
    })
  }

  const obtenerLigas = async () => {
    let response = await listLeagues()

    response && setLeagues(response)
  }

  const obtenerTemporadas = async () => {
    let response = await listSeasons()

    response && setSeasons(response)
  }

  const obtenerJornadas = async () => {
    let response = await listTimes()

    response && setTimes(response)
  }

  const obtenerEquipos = async () => {
    let response = await listTeams()

    response && setTeams(response)
  }

  const obtenerPartidos = async () => {
    let response = await listMatches()

    response && setMatches(response)
  }

  useEffect(() => {
    obtenerPartidos()
    obtenerJornadas()
    obtenerTemporadas()
    obtenerLigas()
    obtenerEquipos()
  }, [])

  return (
    <div style={{ marginTop: '50px' }}>
      <div>
        <h3 style={{ textAlign: 'center', marginBottom: '20px' }}>Partidos por jornada</h3>
      </div>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <div style={{ position: 'relative', padding: '30px', width: '80%' }}>
          <span onClick={agregarPartidosPorJornada} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
            <i class="fa-solid fa-plus"></i> Agregar
          </span>
          {
            matches?.length === 0 ? <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <p>Actualmente no tienes jornadas registradas, ingresa una.</p>
              <span onClick={agregarPartidosPorJornada} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
                <i class="fa-solid fa-plus"></i> Agregar
              </span>
            </div> :
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell align="left">ID liga</StyledTableCell>
                      <StyledTableCell align="right"># de la temporada</StyledTableCell>
                      <StyledTableCell align="right">ID jornada</StyledTableCell>
                      <StyledTableCell align="right">Equipo local</StyledTableCell>
                      <StyledTableCell align="right">Equipo de visita</StyledTableCell>
                      <StyledTableCell align="right"># de jornada original</StyledTableCell>
                      <StyledTableCell align="right">Acciones</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {matches?.length !== 0 && matches?.map((row, key) => (
                      <StyledTableRow key={key}>
                        <StyledTableCell component="th" scope="row">{row.liga_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.temporada_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.jornada_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.local_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.vistante_id}</StyledTableCell>
                        <StyledTableCell align="right">{row.jornada_original}</StyledTableCell>
                        <StyledTableCell align="right">
                          <div>
                            <i style={{ marginRight: '15px' }} onClick={() => { editarPartidosPorJornada(row) }} class="fa-solid fa-pencil"></i>
                            <i class="fa-solid fa-trash-can" onClick={() => { eliminarPartidosPorJornada(row) }}></i>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
          }
        </div>
      </div>
    </div>
  )
}

export default AdminMatches