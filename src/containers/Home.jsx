import React from 'react';
import '../styles/components/Home.css'
import MainImage from '../assets/img/apostamos.png'
import Swal from 'sweetalert2'
import { withRouter } from 'react-router-dom';

import { registerUser } from '../utils/actions'

const Home = () => {
  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  const registerButton = () => {
    Swal.fire({
      title: "Registro",
      html: `
      <input type="text" id="nombre" class="swal2-input" placeholder="Nombre">
      <input type="text" id="username" class="swal2-input" placeholder="Usuario">
      <input type="email" id="email" class="swal2-input" placeholder="Correo electronico">
      <input type="password" id="password" class="swal2-input" placeholder="Contraseña">
      <input type="password" id="password-confirm" class="swal2-input" placeholder="Confirme contraseña">
      <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
        <p>Politicas de privacidad</p>
        <label><input style="margin-left: 15px;" type="checkbox" id="confirm-policy" /> Aceptar</label>
      </div>
      `,
      confirmButtonText: "Registrar",
      focusConfirm: false,
      preConfirm: () => {
        const nombre = Swal.getPopup().querySelector("#nombre").value;
        const username = Swal.getPopup().querySelector("#username").value;
        const email = Swal.getPopup().querySelector("#email").value;
        const password = Swal.getPopup().querySelector("#password").value;
        const passwordConfirm = Swal.getPopup().querySelector("#password-confirm").value;
        const confirmPolicy = Swal.getPopup().querySelector("#confirm-policy").checked;

        // Validations
        if (!nombre || !password || !username || !email || !passwordConfirm) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }
        if (password !== passwordConfirm) { Swal.showValidationMessage(`La contraseña no es la misma, intente de nuevo.`) }
        if (!confirmPolicy) { Swal.showValidationMessage(`Debe aceptar las politícas de privacidad.`) }
        if (!validateEmail(email)) { Swal.showValidationMessage(`Ingresa un correo válido.`) }
        return { nombre, password, username, email };
      },
    }).then((result) => {
      if(result.value) registerUser(result.value)
    });
  }

  return (
    <>
      <main>
        <section className="banner">
          <img src={MainImage} alt="" />
          <div className="contenedor">
            <h1 className="">PARTICIPA GRATIS</h1>
            <br />
            <a onClick={registerButton} className="btn">REGISTRATE</a>
          </div>
        </section>
      </main>
    </>
  );
}

export default withRouter(Home);