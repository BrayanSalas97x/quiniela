import React, { useState, useEffect } from 'react'
import Swal from 'sweetalert2'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { listSeasons, listLeagues, registerSeason, updateSeason, deleteSeason } from '../utils/actions'

const AdminSeasons = () => {
  const [seasons, setSeasons] = useState([])
  const [leagues, setLeagues] = useState([])


  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const agregarTemporada = (row) => {
    Swal.fire({
      title: "Temporada",
      html: `
            <h5># de la Temporada</h5>
            <input type="text" style="width:58%; margin: 1px;" id="num_temporada"    class="swal2-input" placeholder="2021">
            <h5>Nombre de la Temporada</h5>
            <input type="text"  style="width:58%; margin: 1px;" id="nom_temporada"  class="swal2-input" placeholder="Apertura 2021">
            <h5>Liga</h5>
            <select style="width: 263px; margin: 1px;" name="liga" id="liga" class="swal2-input">
            ${leagues?.map((league) => {
              return (
                `<option value="${league.liga_id}">${league.nombre}</option>`
              )
            })}
            </select>
            <h5>Fecha inicio</h5>
            <input type="date" style="width: 58%; margin: 1px;" id="fecha_inicio"    class="swal2-input" placeholder="Fecha inicio">
            <h5>Fecha fin</h5>
            <input type="date" style="width: 58%; margin: 1px;" id="fecha_fin"    class="swal2-input" placeholder="Fecha Terminacion">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const numTemporada = Swal.getPopup().querySelector("#num_temporada").value;
        const nombreTemporada = Swal.getPopup().querySelector("#nom_temporada").value;
        const liga = Swal.getPopup().querySelector("#liga").value;
        const fechaInicio = Swal.getPopup().querySelector("#fecha_inicio").value;
        const fechaFin = Swal.getPopup().querySelector("#fecha_fin").value;

        if (!numTemporada || !nombreTemporada || !liga || !fechaInicio || !fechaFin) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }

        return { numero: Number(numTemporada), nombre: nombreTemporada, liga_id: Number(liga), f_inicio: fechaInicio, f_fin: fechaFin };
      },
    }).then(async (result) => {
      if (result?.value) {
        await registerSeason(result.value)
        await obtenerTemporadas()
      }
    });
  }

  const editarTemporada = (row) => {
    Swal.fire({
      title: `Editar temporada ${row.name}`,
      html: `
            <h5># de la Temporada</h5>
            <input type="text" style="width:58%; margin: 1px;" id="num_temporada"    class="swal2-input" placeholder="2021" value="${row.numero}">
            <h5>Nombre de la Temporada</h5>
            <input type="text"  style="width:58%; margin: 1px;" id="nom_temporada"  class="swal2-input" placeholder="Apertura 2021" value="${row.nombre}">
            <h5>Liga</h5>
            <select style="width: 263px; margin: 1px;" name="liga" id="liga" class="swal2-input">
            ${leagues?.map((league) => {
              return (
                `<option value="${league.liga_id}" ${row?.liga_id === league?.liga_id && 'selected'}>${league.nombre}</option>`
              )
            })}
            </select>
            <h5>Fecha inicio</h5>
            <input type="date" style="width: 58%; margin: 1px;" id="fecha_inicio"    class="swal2-input" placeholder="Fecha inicio" value="${row.fecha_inicio}">
            <h5>Fecha fin</h5>
            <input type="date" style="width: 58%; margin: 1px;" id="fecha_fin"    class="swal2-input" placeholder="Fecha Terminacion" value="${row.fecha_fin}">`,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const numTemporada = Swal.getPopup().querySelector("#num_temporada").value;
        const nombreTemporada = Swal.getPopup().querySelector("#nom_temporada").value;
        const liga = Swal.getPopup().querySelector("#liga").value;
        const fechaInicio = Swal.getPopup().querySelector("#fecha_inicio").value;
        const fechaFin = Swal.getPopup().querySelector("#fecha_fin").value;

        if (!numTemporada || !nombreTemporada || !liga || !fechaInicio || !fechaFin) { Swal.showValidationMessage(`Porfavor rellene todos los campos.`) }

        return { numero: Number(numTemporada), nombre: nombreTemporada, liga_id: Number(liga), f_inicio: fechaInicio, f_fin: fechaFin, temporada_id: row.temporada_id };
      },
    }).then(async (result) => {
      if (result?.value) {
        await updateSeason(result.value)
        await obtenerTemporadas()
      }
    });
  }

  const eliminarTemporada = (row) => {
    Swal.fire({
      title: `Quieres eliminar esta temporada (${row.nombre})?`,
      showCancelButton: true,
      showDenyButton: false,
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar',
    }).then(async (result) => {
      if (result?.value) {
        await deleteSeason(row.temporada_id)
        await obtenerTemporadas()
      }
    })
  }

  const obtenerTemporadas = async () => {
    let response = await listSeasons()

    response && setSeasons(response)
  }

  const obtenerLigas = async () => {
    let response = await listLeagues()

    response && setLeagues(response)
  }

  useEffect(() => {
    obtenerLigas()
    obtenerTemporadas()
  }, [])

  return (
    <div style={{ marginTop: '50px' }}>
      <div>
        <h3 style={{ textAlign: 'center', marginBottom: '20px' }}>Temporadas</h3>
      </div>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <div style={{ position: 'relative', padding: '30px', width: '80%' }}>
          <span onClick={agregarTemporada} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
            <i class="fa-solid fa-plus"></i> Agregar
          </span>
          {
            seasons?.length === 0 ?
              <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <p>Actualmente no tienes temporadas registradas, ingresa una.</p>
                <span onClick={agregarTemporada} style={{ position: 'absolute', top: '0px', right: '30px', cursor: 'pointer' }}>
                  <i class="fa-solid fa-plus"></i> Agregar
                </span>
              </div> :
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell align="left">Numero de la Temporada</StyledTableCell>
                      <StyledTableCell align="right">Nombre de la temporada</StyledTableCell>
                      <StyledTableCell align="right">Fecha de inicio</StyledTableCell>
                      <StyledTableCell align="right">Fecha de fin</StyledTableCell>
                      <StyledTableCell align="right">Acciones</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {seasons?.length !== 0 && seasons?.map((row) => (
                      <StyledTableRow key={row.nombre}>
                        <StyledTableCell component="th" scope="row">{row.numero}</StyledTableCell>
                        <StyledTableCell align="right">{row.nombre}</StyledTableCell>
                        <StyledTableCell align="right">{row.fecha_inicio}</StyledTableCell>
                        <StyledTableCell align="right">{row.fecha_fin}</StyledTableCell>
                        <StyledTableCell align="right">
                          <div>
                            <i style={{ cursor: 'pointer', marginRight: '15px' }} onClick={() => editarTemporada(row)} class="fa-solid fa-pencil"></i>
                            <i style={{ cursor: 'pointer' }} class="fa-solid fa-trash-can" onClick={() => eliminarTemporada(row)}></i>
                          </div>
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
          }
        </div>
      </div>
    </div>
  )
}

export default AdminSeasons