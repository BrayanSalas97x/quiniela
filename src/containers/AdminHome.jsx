import React, { useRef } from 'react';
import LogoImage from '../assets/img/logo-apostamos.png'
import { withRouter } from 'react-router-dom';
import '../styles/components/AdminHome.css'

const AdminHome = (props) => {
  const emailRef = useRef(null)

  const adminLogin = () => {
    localStorage.setItem("admin", emailRef.current.value);

    props.history.push('/admin/dashboard')
  }

  return (
    <div className="main-home">
      <div className="card-login">
        <div className="card-login-container">
          <img src={LogoImage} />
          <p className="card-instructions">Ingresa las credenciales de administrador</p>
          <input ref={emailRef} type={'text'} id="login" className="swal2-input" placeholder="Usuario"></input>
          <input type={'password'} id="password" className="swal2-input" placeholder="Password"></input>
          <button onClick={adminLogin} type="button" className="card-button-login">ENTRAR</button>
        </div>
      </div>
    </div>
  );
}

export default withRouter(AdminHome);