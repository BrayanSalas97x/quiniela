import axios from 'axios'
import Swal from 'sweetalert2'

const url = 'http://localhost:3000/'
const mainUrl = `${url}api/`

const loginUser = async (data) => {
  try {
    let loginResponse = await axios({
      method: 'POST',
      url: `${mainUrl}auth/login`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    localStorage.setItem("usuario", data.username);
    localStorage.setItem("token", loginResponse.data.token);

    return { logged: true }
  } catch (e) {
    Swal.fire({ title: 'No se pudo iniciar sesión', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const registerUser = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}auth/signin`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Registro exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
  } catch (e) {
    Swal.fire({ title: 'Registro fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const listLeagues = async (data) => {
  try {
    let list = await axios({
      method: 'GET',
      url: `${mainUrl}liga/list`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    return list?.data?.data ? list?.data?.data : []
  } catch (e) {
    console.log(e.response)
  }
}

const registerLeague = async (data) => {
  try {
    let formData = new FormData()
    formData.append('nombre', data.nombre)
    formData.append('logo', data.logo)
    formData.append('activo', data.active)

    let register = await axios({
      method: 'POST',
      url: `${mainUrl}liga/create`,
      data: formData,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    })

    Swal.fire({ title: 'Registro exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Registro fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const updateLeague = async (data) => {
  try {
    let formData = new FormData()
    formData.append('nombre', data.nombre)
    formData.append('logo', data.logo)
    formData.append('activo', data.active)
    formData.append('id', data.id)

    let register = await axios({
      method: 'POST',
      url: `${mainUrl}liga/update`,
      data: formData,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    })

    Swal.fire({ title: 'Edición exitosa', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listLeagues()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Edición fallida', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const deleteLeague = async (data) => {
  try {
    let register = await axios({
      method: 'DELETE',
      url: `${mainUrl}liga/delete/${data.id}`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Borrado exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listLeagues()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Borrado fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const listSeasons = async (data) => {
  try {
    let list = await axios({
      method: 'GET',
      url: `${mainUrl}temporada/list`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    return list?.data?.data ? list?.data?.data : []
  } catch (e) {
    console.log(e.response)
  }
}

const registerSeason = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}temporada/create`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Registro exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Registro fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

// No me funciona me dice que hay un error, parece algo interno de la API
const updateSeason = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}temporada/update`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Edición exitosa', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listSeasons()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Edición fallida', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const deleteSeason = async (data) => {
  try {
    let register = await axios({
      method: 'DELETE',
      url: `${mainUrl}temporada/delete/${data}`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Borrado exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listSeasons()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Borrado fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const listTeams = async (data) => {
  try {
    let list = await axios({
      method: 'GET',
      url: `${mainUrl}equipo/list`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    return list?.data?.data ? list?.data?.data : []
  } catch (e) {
    console.log(e.response)
  }
}

const registerTeam = async (data) => {
  try {
    let formData = new FormData()
    formData.append('liga_id', data.liga_id)
    formData.append('logo', data.logo)
    formData.append('temporada_id', data.temporada_id)
    formData.append('nombre', data.nombre)
    formData.append('activo', data.activo)

    let register = await axios({
      method: 'POST',
      url: `${mainUrl}equipo/create`,
      data: formData,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    })

    Swal.fire({ title: 'Registro exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Registro fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

// No me funciona me dice que hay un error, parece algo interno de la API
const updateTeam = async (data) => {
  let formData = new FormData()
    formData.append('equipo_id', data.equipo_id)
    formData.append('liga_id', data.liga_id)
    formData.append('logo', data.logo)
    formData.append('temporada_id', data.temporada_id)
    formData.append('nombre', data.nombre)
    formData.append('activo', data.activo)
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}equipo/update`,
      data: formData,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      }
    })

    Swal.fire({ title: 'Edición exitosa', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listTeams()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Edición fallida', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const deleteTeam = async (data) => {
  try {
    let register = await axios({
      method: 'GET',
      url: `${mainUrl}equipo/delete/${data}`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Borrado exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listTeams()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Borrado fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const listTimes = async (data) => {
  try {
    let list = await axios({
      method: 'GET',
      url: `${mainUrl}jornada/list`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    return list?.data?.data ? list?.data?.data : []
  } catch (e) {
    console.log(e.response)
  }
}

const registerTime = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}jornada/create`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Registro exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Registro fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

// No me funciona me dice que hay un error, parece algo interno de la API
const updateTime = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}jornada/update`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Edición exitosa', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listTimes()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Edición fallida', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const deleteTime = async (data) => {
  try {
    let register = await axios({
      method: 'GET',
      url: `${mainUrl}jornada/delete/${data}`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Borrado exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listTimes()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Borrado fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const listMatches = async (data) => {
  try {
    let list = await axios({
      method: 'GET',
      url: `${mainUrl}partido/list`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    return list?.data?.data ? list?.data?.data : []
  } catch (e) {
    console.log(e.response)
  }
}

const registerMatch = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}partido/create`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Registro exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Registro fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

// No me funciona me dice que hay un error, parece algo interno de la API
const updateMatch = async (data) => {
  try {
    let register = await axios({
      method: 'POST',
      url: `${mainUrl}partido/update`,
      data,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Edición exitosa', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listMatches()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Edición fallida', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

const deleteMatch = async (data) => {
  try {
    let register = await axios({
      method: 'GET',
      url: `${mainUrl}partido/delete/${data}`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })

    Swal.fire({ title: 'Borrado exitoso', icon: 'success', showConfirmButton: false, text: register.data.message, timer: 2000 })
    listMatches()
  } catch (e) {
    console.log(e)
    Swal.fire({ title: 'Borrado fallido', icon: 'error', showConfirmButton: false, text: e.response.data.error, timer: 2000 })
  }
}

export {
  url,
  registerUser,
  loginUser,
  registerLeague,
  listLeagues,
  updateLeague,
  deleteLeague,
  listSeasons,
  registerSeason,
  updateSeason,
  deleteSeason,
  listTeams,
  registerTeam,
  updateTeam,
  deleteTeam,
  listTimes,
  registerTime,
  updateTime,
  deleteTime,
  listMatches,
  registerMatch,
  updateMatch,
  deleteMatch
}