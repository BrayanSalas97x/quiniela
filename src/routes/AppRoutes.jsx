import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from '../containers/Home';
import Dashboard from '../containers/Dashboard';
import AdminHome from '../containers/AdminHome';
import AdminDashboard from '../containers/AdminDashboard';
import AdminLeagues from '../containers/AdminLeagues';
import AdminMatches from '../containers/AdminMatches';
import AdminSeasons from '../containers/AdminSeasons';
import AdminTeams from '../containers/AdminTeams';
import AdminTimes from '../containers/AdminTimes';
import NotFound from '../containers/NotFound';
import Layout from '../components/Layout';
import AppContext from '../context/AppContext';
import useInitialState from '../hooks/useInitialState';

const AppRoutes = () => {
  const initialState = useInitialState();

  return (
    <AppContext.Provider value={initialState}>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/admin/dashboard" component={AdminDashboard} />
            <Route exact path="/admin/dashboard/ligas" component={AdminLeagues} />
            <Route exact path="/admin/dashboard/temporadas" component={AdminSeasons} />
            <Route exact path="/admin/dashboard/equipos" component={AdminTeams} />
            <Route exact path="/admin/dashboard/jornadas" component={AdminTimes} />
            <Route exact path="/admin/dashboard/partidos-por-jornadas" component={AdminMatches} />
            <Route exact path="/admin" component={AdminHome} />
            <Route component={NotFound} />
          </Switch>
        </Layout>
      </BrowserRouter>
    </AppContext.Provider>
  );
}

export default AppRoutes;