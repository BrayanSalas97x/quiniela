import React, { useContext } from 'react';
import AppContext from '../context/AppContext'
import LogoImage from '../assets/img/logo-apostamos.png'
import '../styles/components/Header.css'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { withRouter } from 'react-router-dom'

import { loginUser } from '../utils/actions'

const Header = (props) => {
  const MySwal = withReactContent(Swal)
  const { state } = useContext(AppContext)
  const { cart } = state

  const forgetPasswordButton = () => {
    console.log('funcione...')
    Swal.fire({
      title: "Olvide mi password",
      html: `<p style="margin-top: 15px; font-size: 15px; color: blue;">Ingresa el email para enviar un correo de recuperación</p>
          <input type="text" id="email" class="swal2-input" placeholder="Email">
          `,
      confirmButtonText: "Enviar",
      focusConfirm: false,
      preConfirm: () => {
        const email = Swal.getPopup().querySelector("#email").value;
        if (!email) {
          Swal.showValidationMessage(
            'Porfavor introduzca un email'
          );
        }
        return { email: email };
      },
    }).then((result) => {
      Swal.fire(
        'Correo enviado',
        'Las instrucciones para recuperar tu cuenta han sido enviadas al correo introducido.',
        'success'
      )
    });
  }

  const loginButton = () => {
    MySwal.fire({
      title: "Iniciar sesión",
      html: <div>
          <input type="text" id="username" class="swal2-input" placeholder="Usuario" />
          <input type="password" id="password" class="swal2-input" placeholder="Contraseña" />
          <p onClick={forgetPasswordButton} style={{ marginTop: '15px', fontSize: '15px', color: 'blue', cursor: 'pointer'}}>Olvide mi contraseña</p>
      </div>,
      confirmButtonText: "Ingresar",
      focusConfirm: false,
      preConfirm: () => {
        const username = Swal.getPopup().querySelector("#username").value;
        const password =
          Swal.getPopup().querySelector("#password").value;
        if (!username || !password) {
          Swal.showValidationMessage(
            `Porfavor introduzca un usuario y contraseña`
          );
        }
        return { username, password };
      },
    }).then(async (result) => {
      if(result.value) {
        let loginResponse = await loginUser(result.value)

        if(loginResponse?.logged) {
          props.history.push('/dashboard')
        }
      }
    });
  }

  const logoutButton = () => {
    if(localStorage.getItem('usuario')) {
      localStorage.removeItem('usuario') 
      props.history.push('/')
    } else {
      localStorage.removeItem('admin')
      props.history.push('/admin')
    }
  }

  return (
    <div class="container">
      <div class="middle">
        <img src={LogoImage} style={{ width: '299px' }} />
      </div>
      {
        localStorage.getItem('usuario') || localStorage.getItem('admin') ?
          <nav class="menu">
            <p class="ov-btn-slide-right">HOLA, {localStorage.getItem('usuario') ? localStorage.getItem('usuario').toUpperCase() : localStorage.getItem('admin').toUpperCase()}</p>
            <p class="ov-btn-slide-right" onClick={logoutButton}>CERRAR SESIÓN</p>
          </nav> :
          <nav class="menu">
            <p class="ov-btn-slide-right">INICIO</p>
            <p class="ov-btn-slide-right">POSTA DEPORTES</p>
            <p class="ov-btn-slide-right">POSTA.COM.MX</p>
            <p class="ov-btn-slide-right" onClick={loginButton}>INICIAR SESIÓN</p>
          </nav>
      }
    </div>
  );
}

export default withRouter(Header)


