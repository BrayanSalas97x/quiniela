import React from 'react';
import Header from './Header';
import Footer from './Footer';
import { withRouter } from 'react-router-dom';
import '../styles/components/Layout.css'

const Layout = ({ children, history }) => {
  console.log('history', history.location.pathname)

  return (
    <>
      {history.location.pathname !== '/admin' && <Header />}
      {children}
      {/* <Footer /> */}
    </>
  );
}

export default withRouter(Layout);